let cellsMap = [];
let neuralNetwork = null;
let trainData = [];
let painter = null;
let inputAssociation = null;

document.addEventListener("DOMContentLoaded", InitApplication);


function InitApplication() {
  painter = new Painter(document.getElementById('canv'));

  inputAssociation = document.getElementById('inpAss');

  document.addEventListener('keypress', function (el) {
    switch (el.key) {
      /* Очистка холста */
      case '1':
        painter.clearCanvas();
        break;
      /* Вычисление карты ячеек и добавление тренировочных данных */
      case '2':
        addTrainData();
        break;

      /* Распознавание рисунка */
      case '3':
        neuralNetwork = new brain.NeuralNetwork();
        neuralNetwork.train(trainData, {log: true});

        const result = brain.likely(painter.calculate(), neuralNetwork);
        alert(result);
        break;
    }
  })
}

/* Вычисление карты ячеек и добавление тренировочных данных */
function addTrainData() {
  const newTraiData = {
    input: painter.calculate(true),
    output: {}
  };
  newTraiData.output[inputAssociation.value] = 1;

  trainData.push(newTraiData);
}

function Painter(canv) {
  const ctx = canv.getContext('2d');
  const pixel = 20;

  let isMouseDown = false;

  canv.width = 500;
  canv.height = 500;
  ctx.lineJoin = 'miter';

  const w = canv.width;
  const h = canv.height;
  const p = w / pixel;

  const xStep = w / p;
  const yStep = h / p;

  this.drawLine = function(x1, y1, x2, y2) {
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
  }

  this.drawCell = function (x, y, w, h) {
    ctx.rect(x, y, w, h);
    ctx.fill();
  }

  this.clearCanvas = function () {
    ctx.clearRect(0, 0, w, h);
  }

  this.drawGrid = function () {
    ctx.strokeStyle = 'gray';

    for(let x = 0; x < w; x += xStep){
      this.drawLine(x, 0, x, h);
    }

    for(let y = 0; y < h; y += yStep){
      this.drawLine(0, y, w, y);
    }
  }

  this.calculate = function (draw = false) {
    const cellsMap = [];
    let __draw = [];
    let data = null;
    let dateLength = 0;
    let nonEmptyPixels = false;

    for(let x = 0; x < w; x += xStep) {
      for(let y = 0; y < h; y += yStep) {
        data = ctx.getImageData(x, y, xStep, yStep).data;
        dateLength = data.length;
        nonEmptyPixels = false;

        for(let i = 0; i < dateLength; i += 10) {
          if (data[i] !== 0) {
            __draw.push([x, y, xStep, yStep]);
            nonEmptyPixels = true;
            break;
          }
        }

        cellsMap.push(nonEmptyPixels ? 1 : 0);
      }
    }

    if (draw) {
      this.clearCanvas();
      this.drawGrid();

      ctx.fillStyle = 'green';
      ctx.strokeStyle = 'green';

      for(_d in __draw) {
        this.drawCell(__draw[_d][0], __draw[_d][1], __draw[_d][2], __draw[_d][3]);
      }
    }

    return cellsMap;
  }

  canv.addEventListener('mousedown', function () {
    isMouseDown = true;
    ctx.fillStyle = 'red';
    ctx.strokeStyle = 'red';
    ctx.lineWidth = pixel;
    ctx.beginPath();
  })

  canv.addEventListener('mouseup', function () {
    isMouseDown = false;
    ctx.lineWidth = 1;
  })

  canv.addEventListener('mousemove', function (e) {
    if (isMouseDown) {
      ctx.lineTo(e.offsetX, e.offsetY);
      ctx.stroke();

      ctx.beginPath();
      ctx.arc(e.offsetX, e.offsetY, pixel / 2, 0, Math.PI * 2);
      ctx.fill();

      ctx.beginPath();
      ctx.moveTo(e.offsetX, e.offsetY);
    }
  })
}
